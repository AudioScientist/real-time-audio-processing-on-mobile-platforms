### Version 2.0 ###
* Date : 2020-01-05
* Author : Thomas Hézard - thomas.hezard@thz.fr

* Change log :
    - Include `AudioFile` library in S2 in order to work directly with WAV audio files.
    - Update documentation.
    - Prepare repl.it deployment.
    
*****


### Version 1.0 ###
* Date : 2019-02-18
* Author : Thomas Hézard - thomas.hezard@thz.fr

* Change log :
    - First public version
    
*****