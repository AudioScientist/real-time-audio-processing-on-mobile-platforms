# Offline Audio Processor

## Introduction

This project is the step 1 of the main project [Real-time audio processing on mobile platforms](https://gitlab.com/AudioScientist/real-time-audio-processing-on-mobile-platforms) written for Matlab. Please refer to the README of main project for more information.

--- 

## Author

Thomas Hézard - Audio scientist and developer
www.thomashezard.com  
thomas.hezard [at] thz.fr

--- 

## License

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).  
[![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

---

## Objectives

This folder contains a self-sufficient Matlab script `audio_processor.m`. The present version of the code does nothing but copying audio data from an input audio file into an output audio file.  
Your goal is to implement modify this script to replace the data copy with the audio processing algorithm you chose. All you have to do is to __replace [line 41](audio_processor.m#L41) with your code__.

--- 

## Matlab environment

This project has been written and tested with Matlab R2018a. It does require any additional toolbox.
